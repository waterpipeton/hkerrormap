// React
import React, { Component } from 'react'
import { Switch, Route } from "react-router-dom";
import { Authenticator } from 'aws-amplify-react';
import readingTime from 'reading-time';
// Material-UI
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import Snackbar from '@material-ui/core/Snackbar';

// Custom
import colors from '../colors';
import settings from '../settings';


import Bar from '../layout/Bar/Bar';

import HomeScreen from '../screens/Home/Home';
import MapScreen from '../screens/Map';
import MapPlaybackScreen from '../screens/MapPlayback';
import NotFoundScreen from '../screens/NotFound/NotFound';

let theme = createMuiTheme({
  palette: {
    primary: settings.theme.primaryColor.import,
    secondary: settings.theme.secondaryColor.import,
    type: settings.theme.type
  }
});

class App extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      primaryColor: settings.theme.primaryColor.name,
      secondaryColor: settings.theme.secondaryColor.name,
      type: settings.theme.type,
      isSignedIn: false,
      awsUI: false,
      user: null,
      auth: 'init',
      snackbar: {
        autoHideDuration: 0,
        message: '',
        open: false
      }
    };
  }

  updateTheme = (palette, removeLocalStorage, callback) => {
    const { primaryColor, secondaryColor, type } = this.state;

    if (!palette.primaryColor) {
      palette.primaryColor = primaryColor;
    }

    if (!palette.secondaryColor) {
      palette.secondaryColor = secondaryColor;
    }

    if (!palette.type) {
      palette.type = type;
    }

    theme = createMuiTheme({
      palette: {
        primary: colors.find(color => color.id === palette.primaryColor).import,
        secondary: colors.find(color => color.id === palette.secondaryColor).import,
        type: palette.type
      }
    });

    this.setState({
      primaryColor: palette.primaryColor,
      secondaryColor: palette.secondaryColor,
      type: palette.type
    }, () => {
      if (removeLocalStorage) {
        localStorage.removeItem('theme');
      } else {
        localStorage.setItem('theme', JSON.stringify({
          primaryColor: palette.primaryColor,
          secondaryColor: palette.secondaryColor,
          type: palette.type
        }));
      }

      if (callback && typeof callback === 'function') {
        callback();
      }
    });
  };

  resetTheme = () => {
    this.updateTheme({
      primaryColor: settings.theme.primaryColor.name,
      secondaryColor: settings.theme.secondaryColor.name,
      type: settings.theme.type
    }, true, () => {
      this.openSnackbar('Settings reset');
    });
  };

  changePrimaryColor = (event) => {
    const primaryColor = event.target.value;

    this.updateTheme({
      primaryColor
    });
  };

  changeSecondaryColor = (event) => {
    const secondaryColor = event.target.value;

    this.updateTheme({
      secondaryColor
    });
  };

  changeType = (event) => {
    const type = event.target.value;

    this.updateTheme({
      type
    });
  };

  openSnackbar = (message) => {
    this.setState({
      snackbar: {
        autoHideDuration: readingTime(message).time * 2,
        message,
        open: true
      }
    });
  };

  closeSnackbar = (clearMessage = false) => {
    const { snackbar } = this.state;

    this.setState({
      snackbar: {
        message: clearMessage ? '' : snackbar.message,
        open: false
      }
    });
  };

  handleAuthState = (state, data) => {
    this.setState({ auth: state, authData: data });
  }

  openAWSUI = () => {
    this.setState(preState => ({ awsUI: !preState.awsUI }));
  }

  render() {
    const {
      isSignedIn,
      user,
    } = this.state;

    const { snackbar } = this.state;

    return <MuiThemeProvider theme={theme}>
      <div style={{ minHeight: '100vh', backgroundColor: theme.palette.type === 'dark' ? '#303030' : '#fafafa' }}>

        <React.Fragment>
          <Bar
            title={settings.title}

            isSignedIn={isSignedIn}
            isPerformingAuthAction={this.state.awsUI}

            user={user}

            onSignInClick={this.openAWSUI}

            onSettingsClick={this.openSettingsDialog}
            onSignOutClick={this.openSignOutDialog}
          />
          {
            !this.state.awsUI &&
            <Switch>
              <Route path="/" exact render={() => (<HomeScreen isSignedIn={isSignedIn} title={settings.title} />)} />
              <Route path="/maps/:mapId" exact render={(props) => (<MapScreen user={{} || user} isSignedIn={true || isSignedIn} {...props} />)} />
              <Route path="/playback" exact render={(props) => (<MapPlaybackScreen user={{} || user} isSignedIn={true || isSignedIn} {...props} />)} />
              <Route component={NotFoundScreen} />
            </Switch>
          }
          <Snackbar
            autoHideDuration={snackbar.autoHideDuration}
            message={snackbar.message}
            open={snackbar.open}
            onClose={this.closeSnackbar}
          />
          {
            this.state.awsUI && <Authenticator onStateChange={this.handleAuthState} />
          }
        </React.Fragment>
      </div>
    </MuiThemeProvider>;
  }

  componentDidMount() {
    this._isMounted = true;

    const theme = JSON.parse(localStorage.getItem('theme'));

    if (theme) {
      this.updateTheme(theme);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
}

export default App;
