import React from 'react';
import { connect } from 'react-redux';
import { Typography, Grid } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { Marker as LeafletMarker, Popup } from 'react-leaflet';
import TimeAgo from 'timeago-react';
import { emojiIndex } from 'emoji-mart';
import { SHOW_INFOWINDOW, CLOSE_INFOWINDOW, START_EDIT_MARKER } from '../redux/reducers/screenReducer';
import leaflet from 'leaflet';

function getEmojiHtml(emoji) {
  return `
<span style="text-align: center; line-height: 20px; font-size: 30px;">
  ${emoji ? emoji.native : ''}
</span>
  `;
}

const getEmoji = (id) => {
  const foundEmoji = emojiIndex.search(id).filter(m => m.id === id);
  if (foundEmoji.length > 0) {
    return foundEmoji[0];
  }
  return null;
}

// represent Marker and the InfoWindow
class Marker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mapId: window.location.pathname.split('/').filter(p => p !== '').pop(),
    };
    // this.deleteMarker = this.deleteMarker.bind(this);
  }

  deleteMarker() {
    // const { marker } = this.props;
    // const { mapId } = this.state;
    // db.collection('maps').doc(mapId).collection('markers').doc(marker.markerId).delete()
    //   .then(() => console.log('marker deleted'))
    //   .catch((error) => console.error('marker delete error', error));
  }

  render() {
    const { marker, layer } = this.props;
    let emojiIcon = null;
    if (layer && layer.emojiIconId) {
      emojiIcon = getEmoji(layer.emojiIconId);
    } else {
      emojiIcon = getEmoji('question');
    }
    return (
      <LeafletMarker key={marker.id} position={[marker.position.latitude, marker.position.longitude]}
        icon={leaflet.divIcon({
          html: getEmojiHtml(emojiIcon),
          iconSize: [20, 20],
        })}
        onClick={() => this.props.editingMarker(marker.id)}
      // onMouseOver={() => this.props.showInfoWindow(marker.markerId)}
      >
        {this.props.showMarkerId === marker.id &&
          <Popup onCloseClick={this.props.closeInfoWindow}>
            <Grid>
              <Grid >
                <Typography variant="subtitle1">
                  {marker.title || 'No Title'}
                  <EditIcon onClick={() => this.props.editingMarker(marker.id)} />
                </Typography>
                {marker.description}
                <Typography variant="subtitle2">
                  <TimeAgo
                    datetime={new Date(marker.updatedAt * 1000)}
                    locale='en-US' />
                </Typography>
              </Grid>
            </Grid>
          </Popup>}
      </LeafletMarker>
    )
  }
}

const mapStateToProps = (state) => ({
  showMarkerId: state.screen.map.showMarkerId,
});
const mapDispatchToProps = (dispatch) => ({
  showInfoWindow: (markerId) => dispatch({ type: SHOW_INFOWINDOW, markerId }),
  closeInfoWindow: () => dispatch({ type: CLOSE_INFOWINDOW }),
  editingMarker: (markerId) => dispatch({ type: START_EDIT_MARKER, markerId }),
});
export default connect(mapStateToProps, mapDispatchToProps)(Marker);