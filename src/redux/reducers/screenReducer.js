import { combineReducers } from 'redux';
import produce from "immer";

const reducerName = '[reducer:/screen/map]'

export const START_CREATE_MARKER = `${reducerName} Start Creating Marker`;
export const END_CREATE_MARKER = `${reducerName} End Creating Marker`;
export const TOGGLE_LOCATION_PICKER = 'MAP_SCREEN_TOGGLE_LOCATION_PICKER';
export const SHOW_INFOWINDOW = `${reducerName} Show Marker InfoWindow`;
export const CLOSE_INFOWINDOW = `${reducerName} Close Marker InfoWindow`;
export const MARKERS_CHANGE = `${reducerName} Change Markers`;
export const LAYERS_CHANGE = `${reducerName} Change Layers`;
export const MAPS_CHANGE = `${reducerName} Change Maps`;

// TODO: finish the function in MapScreen
export const START_EDIT_MARKER = `${reducerName} Start Editing Marker`;
export const END_EDIT_MARKER = `${reducerName} End Editing Marker`;


const mapInitialState = {
  // flag to represent the process of creating the marker
  // at this state the locationPicker is dropped but not yet save
  // also user is choosing icon and inputing texts
  // Map is rendering the location picker, and the control box is shown to key in details.
  // the details is not yet save to database though.
  // when save, this flag should turn back off.
  creatingMarker: false,
  // when creating marker, a Google Map Pin is rendered to represent the to-be marker position
  // those coordinates are saved in this variable
  pickerPosition: null,
  // --------- end of creatingMarker ---------

  // similar to creatingMarker, represent the state of updating but not yet save
  // markerId will be stored in this variable
  editingMarker: false,
  // store the dragged marker position before save button is hit
  stagingMarkerPosition: null,

  // stores all the rendering markers
  // markerId serve as key to reduce lookup time
  markers: [],
  // the marker that user chosen to see the InfoWindow
  showMarkerId: null,
  layers: [],
  maps: [],
}

const mapScreenReducer = (state = mapInitialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case START_CREATE_MARKER:
        return {
          ...state,
          creatingMarker: true,
          editingMarker: null,
          pickerPosition: action.pickerPosition
        };
      case END_CREATE_MARKER:
        return {
          ...state,
          creatingMarker: false,
          editingMarker: null,
          pickerPosition: null
        };

      case MARKERS_CHANGE:
        switch (action.changeType) {
          case 'created':
            action.markers.forEach(m => draft.markers.push(m));
            return;
          case 'updated':
            action.markers.forEach(mm => {
              const i = draft.markers.findIndex(m => m.id === mm.id);
              if (i >= 0) {
                draft.markers[i] = mm;
              } else {
                draft.markers.push(mm);
              }
            });
            return;
          case 'deleted':
            action.markers.forEach(mm => {
              draft.markers.splice(draft.markers.findIndex(m => m.id === mm.id), 1);
            });
            return;
          default:
            return;
        }
      // control marker infowindow
      case SHOW_INFOWINDOW:
        return {
          ...state,
          showMarkerId: action.markerId,
        };
      case CLOSE_INFOWINDOW:  // TODO: change to action type
        return {
          ...state,
          showMarkerId: null,
        };

      // marker editing
      case START_EDIT_MARKER:
        return {
          ...state,
          creatingMarker: false,
          editingMarker: action.markerId,
        }
      case END_EDIT_MARKER:
        return {
          ...state,
          creatingMarker: false,
          editingMarker: null,
        };

      case MAPS_CHANGE:
        switch (action.changeType) {
          case 'created':
          case 'updated':
            action.maps.forEach(mm => {
              const i = draft.maps.findIndex(m => m.id === mm.id);
              if (i >= 0) {
                draft.maps[i] = mm;
              } else {
                draft.maps.push(mm);
              }
            });
            return;
          case 'deleted':
            action.maps.forEach(mm => {
              draft.maps.splice(draft.maps.findIndex(m => m.id === mm.id), 1);
            });
            return;
          default:
            return;
        }
      case LAYERS_CHANGE:
        switch (action.changeType) {
          case 'created':
          case 'updated':
            action.layers.forEach(ll => {
              const i = draft.layers.findIndex(l => l.id === ll.id);
              if (i >= 0) {
                draft.layers[i] = ll;
              } else {
                draft.layers.push(ll);
              }
            });
            return;
          case 'deleted':
            action.layers.forEach(ll => {
              draft.layers.splice(draft.layers.findIndex(l => l.id === ll.id), 1);
            });
            return;
          default:
            return;
        }
      default:
        return;
    }
  });

const screenReducer = combineReducers({
  map: mapScreenReducer,
})

export default screenReducer;