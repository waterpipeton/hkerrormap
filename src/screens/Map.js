import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { API, graphqlOperation } from 'aws-amplify';
import { makeStyles } from '@material-ui/styles';
import { Map as LeafletMap, FeatureGroup, LayersControl, LayerGroup, Marker as LeafletMarker } from 'react-leaflet';
import { Icon } from 'leaflet';
import PouchDBTileLayer from '../components/PouchDBTileLayer'
import Marker from '../components/Marker';
import MarkerEditPanel from '../components/MarkerEditPanel';
import LaunchScreen from '../layout/LaunchScreen/LaunchScreen';
import removeEmptyStringElements from '../util/removeEmptyStringElements';

import { END_CREATE_MARKER, MARKERS_CHANGE, LAYERS_CHANGE, END_EDIT_MARKER, START_CREATE_MARKER } from '../redux/reducers/screenReducer';

import { getMap, listMarkers, listLayers, getMarker } from '../graphql/queries'
import { onCreateMarker, onUpdateMarker, onDeleteMarker } from '../graphql/subscriptions'
import { createMarker, updateMarker } from '../graphql/mutations';

import 'leaflet/dist/leaflet.css';
// https://github.com/PaulLeCam/react-leaflet/issues/255
// stupid hack so that leaflet's images work after going through webpack
import marker from 'leaflet/dist/images/marker-icon.png';
import marker2x from 'leaflet/dist/images/marker-icon-2x.png';
import markerShadow from 'leaflet/dist/images/marker-shadow.png';
import layers from 'leaflet/dist/images/layers.png';
import layers2x from 'leaflet/dist/images/layers-2x.png';

delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: marker2x,
  iconUrl: marker,
  shadowUrl: markerShadow
});

const useStyles = makeStyles(theme => ({
  mapContainer: {
    height: 'calc(100vh - 64px)',
  },
  map: {
    width: '100%',
    height: '100%',
    zIndex: 0,
  }
}));

const MapScreen = ({
  match,
  isSignedIn,
  user,
  layers,
  maps,
  markers,
  creatingMarker,
  editingMarker,
  pickerPosition,
  setLocationPicker,
  unsetLocationPicker,
  changeLayer,
  changeMarker,
  clearEdit
}) => {
  const classes = useStyles();
  const [map, setMap] = useState(null);
  const [ready, setReady] = useState(false);
  const mapId = match.params.mapId;

  useEffect(() => {
    getData();

    const subscriptionMarker = API.graphql(graphqlOperation(onCreateMarker)).subscribe({
      next: (eventData) => {
        const marker = eventData.value.data.onCreateMarker;
        changeMarker("created", [marker]);
      }
    });
    const subscriptionMarker2 = API.graphql(graphqlOperation(onUpdateMarker)).subscribe({
      next: (eventData) => {
        const marker = eventData.value.data.onUpdateMarker;
        changeMarker("updated", [marker]);
      }
    });
    const subscriptionMarker3 = API.graphql(graphqlOperation(onDeleteMarker)).subscribe({
      next: (eventData) => {
        const marker = eventData.value.data.onDeleteMarker;
        changeMarker("deleted", [marker]);
      }
    });

    return () => {
      subscriptionMarker.unsubscribe();
      subscriptionMarker2.unsubscribe();
      subscriptionMarker3.unsubscribe();
    }
  }, []);

  const getData = async () => {
    const mapData = await API.graphql(graphqlOperation(getMap, { id: mapId }));
    setMap(mapData.data.getMap);
    const markerData = await API.graphql(graphqlOperation(listMarkers))
    changeMarker("created", markerData.data.listMarkers.items);
    const layersData = await API.graphql(graphqlOperation(listLayers))
    changeLayer("created", layersData.data.listLayers.items);
    setReady(true);
  }


  const deleteMarker = () => {
    const id = editingMarker;
    // db.collection('maps').doc(mapId).collection('markers').doc(id)
    //   .delete()
    //   .then(() => {
    //     console.log('marker deleted');
    //   }).catch(error => {
    //     console.error('marker delete failure', error);
    //   })
  }

  const saveCreate = async (values, { setSubmitting, setErrors /* setValues, setStatus, and other goodies */ }) => {

    const { title, description, emojiIconId } = values;
    const timestamp = Math.round(new Date().getTime() / 1000);
    const layer = layers.find(l => l.value === values.layer)
    if (isSignedIn && user) {
      const marker = removeEmptyStringElements({
        markerMapId: map.id,
        title: title,
        description: description,
        emojiIconId: emojiIconId,
        position: { latitude: pickerPosition.lat, longitude: pickerPosition.lng },
        markerLayerId: layer.id,
        createdAt: timestamp,
        updatedAt: timestamp,
        createUserID: user.uid,
        createUserEmail: user.email,
      });
      try {
        const newMarker = await API.graphql(graphqlOperation(createMarker, { input: marker }));
        console.log('create marker success. id: ', newMarker.data.createMarker.id);
        unsetLocationPicker();
      } catch (e) {
        setSubmitting(false);
        throw e;
      }
    } else {
      alert('Please Sign In');
    }
  }

  const saveEdit = async (values, { setSubmitting, setErrors /* setValues, setStatus, and other goodies */ }) => {

    const { id, title, description } = values;
    const timestamp = Math.round(new Date().getTime() / 1000);
    const layer = layers.find(l => l.value === values.layer)

    if (isSignedIn && user) {
      const markerData = await API.graphql(graphqlOperation(getMarker, { id: id }));
      const oldMarker = markerData.data.getMarker;

      const marker = removeEmptyStringElements({
        id: id,
        position: oldMarker.position,
        description: description,
        title: title,
        createdAt: oldMarker.createdAt,
        updatedAt: timestamp,
        createUserID: oldMarker.createUserID,
        createUserEmail: oldMarker.createUserEmail,
        updateUserID: user.uid,
        updateUserEmail: user.email,
        markerMapId: oldMarker.map.id,
        markerLayerId: layer.id
      });
      try {
        const upMarker = await API.graphql(graphqlOperation(updateMarker, { input: marker }));
        console.log('update marker success. id: ', upMarker.data.updateMarker.id);
        clearEdit();
      } catch (e) {
        setSubmitting(false);
        throw e;
      }
    } else {
      alert('Please Sign In');
    }
  }

  const mapClick = (event) => {
    if (isSignedIn && user) {
      setLocationPicker(event.latlng)
    }
  }

  // this will render Hong Kong on map
  const defaultCenter = [22.3528, 114.1600];
  const defaultZoom = 11;
  const othersMarkers = markers.filter(m => !m.layer);
  const selectedMarker = editingMarker ? markers.find(m => m.id === editingMarker) : null;
  if (!ready) {
    return <LaunchScreen />;
  }
  return (
    <div>
      <div className={classes.mapContainer}>
        {map &&
          <LeafletMap
            zoom={map ? map.defaultZoom : defaultZoom}
            center={map ? [map.defaultCenter.latitude, map.defaultCenter.longitude] : defaultCenter}
            className={classes.map}
            onClick={mapClick}
          >
            <LayersControl position="topleft">
              <LayersControl.BaseLayer checked name="OSM Color">
                <PouchDBTileLayer
                  useCache={true}
                  crossOrigin={true}
                  attribution='&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
              </LayersControl.BaseLayer>
              <LayersControl.BaseLayer name="OSM Grey Scale">
                <PouchDBTileLayer
                  useCache={true}
                  crossOrigin={true}
                  attribution='&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
                />
              </LayersControl.BaseLayer>
              <LayersControl.BaseLayer name="HK Color">
                <LayerGroup>
                  <PouchDBTileLayer
                    useCache={true}
                    crossOrigin={true}
                    attribution="<a href='https://api.portal.hkmapservice.gov.hk/disclaimer' target='_blank'>&copy; Map from Lands Department <img src='https://api.hkmapservice.gov.hk/mapapi/landsdlogo.jpg' style='width:25px;height:25px'/></a>"
                    // url="https://api.hkmapservice.gov.hk/osm/xyz/basemap/WGS84/tile/{z}/{x}/{y}.png?key=454c6d4b534c40fbb29b2a0845e32a55"
                    url="https://maptile.hkerror.com/h/{z}/{x}/{y}.png"
                 />
                  <PouchDBTileLayer
                    attribution="<a href='https://api.portal.hkmapservice.gov.hk/disclaimer' target='_blank'>&copy; Map from Lands Department <img src='https://api.hkmapservice.gov.hk/mapapi/landsdlogo.jpg' style='width:25px;height:25px'/></a>"
                    url="https://api.hkmapservice.gov.hk/osm/xyz/label-tc/WGS84/tile/{z}/{x}/{y}.png?key=454c6d4b534c40fbb29b2a0845e32a55"
                  />
                </LayerGroup>
              </LayersControl.BaseLayer>
              <LayersControl.BaseLayer name="HK Grey Scale">
                <LayerGroup>
                  <PouchDBTileLayer
                    useCache={true}
                    crossOrigin={true}
                    attribution="<a href='https://api.portal.hkmapservice.gov.hk/disclaimer' target='_blank'>&copy; Map from Lands Department <img src='https://api.hkmapservice.gov.hk/mapapi/landsdlogo.jpg' style='width:25px;height:25px'/></a>"
                    url="https://api.hkmapservice.gov.hk/osm/xyz/basemap/GS/WGS84/tile/{z}/{x}/{y}.png?key=454c6d4b534c40fbb29b2a0845e32a55"
                  />
                  <PouchDBTileLayer
                    useCache={true}
                    crossOrigin={true}
                    attribution="<a href='https://api.portal.hkmapservice.gov.hk/disclaimer' target='_blank'>&copy; Map from Lands Department <img src='https://api.hkmapservice.gov.hk/mapapi/landsdlogo.jpg' style='width:25px;height:25px'/></a>"
                    url="https://api.hkmapservice.gov.hk/osm/xyz/label-tc/WGS84/tile/{z}/{x}/{y}.png?key=454c6d4b534c40fbb29b2a0845e32a55"
                  />
                </LayerGroup>
              </LayersControl.BaseLayer>
              {layers.map(layer =>
                <LayersControl.Overlay checked name={layer.label} key={layer.id}>
                  <FeatureGroup>
                    {markers.filter(m => m.layer && m.layer.id === layer.id).map(marker => <Marker key={marker.id} marker={marker} layer={layer} />)}
                  </FeatureGroup>
                </LayersControl.Overlay>)
              }
              {
                othersMarkers.length > 0 && <LayersControl.Overlay checked name="Others">
                  <FeatureGroup>
                    {
                      othersMarkers.map(marker => <Marker key={marker.id} marker={marker} />)
                    }
                    {/* location picker for creating marker */}
                    {isSignedIn && user && creatingMarker &&
                      <LeafletMarker position={pickerPosition} />}

                  </FeatureGroup>
                </LayersControl.Overlay>
              }
            </LayersControl>

          </LeafletMap>}
      </div>

      {/* Control Box for creating marker */}
      {isSignedIn && user && !editingMarker && creatingMarker && <MarkerEditPanel
        isSignedIn={isSignedIn}
        user={user}
        mapId={mapId}
        saveAction={saveCreate}
        cancelAction={unsetLocationPicker}
      />}
      {editingMarker && <MarkerEditPanel
        isSignedIn={isSignedIn}
        user={user}
        mapId={mapId}
        marker={selectedMarker}
        saveAction={saveEdit}
        cancelAction={clearEdit}
        deleteAction={deleteMarker}
      />}
    </div>
  );
}

MapScreen.propTypes = {
  isSignedIn: PropTypes.bool.isRequired,
  user: PropTypes.object
}

const mapStateToProps = (state) => ({
  creatingMarker: state.screen.map.creatingMarker,
  editingMarker: state.screen.map.editingMarker,
  pickerPosition: state.screen.map.pickerPosition,

  markers: state.screen.map.markers,
  layers: state.screen.map.layers,
});
const mapDispatchToProps = (dispatch) => ({
  setLocationPicker: (latlng) => dispatch({ type: START_CREATE_MARKER, pickerPosition: latlng }),
  unsetLocationPicker: () => dispatch({ type: END_CREATE_MARKER }),
  clearEdit: () => dispatch({ type: END_EDIT_MARKER }),
  changeMarker: (changeType, markers) => dispatch({ type: MARKERS_CHANGE, changeType, markers }),
  changeLayer: (changeType, layers) => dispatch({ type: LAYERS_CHANGE, changeType, layers }),
});
export default connect(mapStateToProps, mapDispatchToProps)(MapScreen);