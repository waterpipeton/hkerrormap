import React, { useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import { API, Auth, graphqlOperation } from 'aws-amplify';

import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { connect } from 'react-redux';
import { MAPS_CHANGE } from '../../redux/reducers/screenReducer';

import MapCard from '../../components/MapCard';

import { listMaps } from '../../graphql/queries'

const useStyles = makeStyles(theme => ({
  container: {
    margin: '40px auto 0',
    maxWidth: '800px',
  },
  card: {
    position: 'relative',
    marginBottom: 20,
  },
}));


const HomeScreen = ({ isSignedIn, title }) => {
  const classes = useStyles();
  const [maps, setMaps] = useState([]);

  useEffect(() => {
    (async () => {
      const anonymousUser = await Auth.currentCredentials();
      await getData();

    })();

    // const subscriptionMap = API.graphql(graphqlOperation(onUpdateMap)).subscribe({
    //   next: (eventData) => {
    //     const map = eventData.value.data.onUpdateMap;
    //     changeMap("updated", [map])
    //   }
    // });
    // const subscriptionMap2 = API.graphql(graphqlOperation(onCreateMap)).subscribe({
    //   next: (eventData) => {
    //     const map = eventData.value.data.onCreateMap;
    //     changeMap("created", [map])
    //   }
    // });

    // return () => {
    //   subscriptionMap.unsubscribe();
    //   subscriptionMap2.unsubscribe();
    // }
  }, []);

  async function getData() {
    try {
      const mapData = await API.graphql(graphqlOperation(listMaps))
      setMaps(mapData.data.listMaps.items);
    } catch (e) {
      throw e;
    }
  }

  return (
    <Grid container className={classes.container}>
      {maps.map((map) => (
        <Grid item xs={12} key={map.id} className={classes.card}>
          <MapCard map={map} />
        </Grid>
      ))}
      <Grid item xs={12} className={classes.card}>
        <MapCard map={{ thumbUrl: '', url: 'playback', id: '', title: 'Playback mode', disabled: true }} />
      </Grid>
    </Grid>
  );
}

HomeScreen.propTypes = {
  isSignedIn: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
};

export default HomeScreen;